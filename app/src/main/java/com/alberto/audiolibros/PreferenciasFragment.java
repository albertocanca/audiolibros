package com.alberto.audiolibros;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by Alberto on 20/01/2017.
 */

public class PreferenciasFragment extends PreferenceFragment {
    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
